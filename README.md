# weblog

#### 项目介绍
基于 Python 语言，使用 Flask 框架实现的个人轻博客。没有使用任何的前端框架及样式。

#### 安装步骤

1. 安装Python3.x环境(如果不存在的话)
2. 安装依赖模块(**requirements.txt**)
3. 配置**config.py**，需要注意**APP_IP**和**APP_PORT**
4. 执行**gendb.py**，生成数据库文件(data.sqlite)
5. 确保**根目录**下存在**visitor.txt**文件，若不存在则创建一个
6. 执行**start.py**，启动服务器

#### 功能模块

- [首页] 最近更新文章
- [归档] 列出分类信息
- [阅读] 在线阅读txt文档
- [句子] 所有经典句子

##### 账号说明

- 默认后台管理地址http://ip:port/admin/
- 默认管理员账号密码admin/123456
- 未提供账号管理功能，请直接修改数据库

#### 软件架构
```
weblog
  |
  |- app
  |   |- static      静态资源路径
  |	  |- template    模板路径
  |   |- data.sqlite 数据库
  |   |- *.py        项目脚本
  |
  |- gendb.py  初始化数据库
  |- config.py 配置文件
  |- start.py  程序入口
  |- visitor.txt 访问计数文件

```

#### 脚本说明
- controller.py 用于控制页面请求跳转
- handler.py    处理访问计数及错误页面
- modelview.py  后台维护视图
- modelform.py  登录表单
- model.py      领域对象
- uploader.py   文件上传
- reading.py    阅读txt文档


#### 静态资源说明
- txt           存放txt文档，注意必须是utf-8编码
- upload        图片上传目录

#### 启动服务命令

查询进程

netstat -anlp | grep 80

后台执行

nohup python3 ./start.py >run.log 2>&1 &
