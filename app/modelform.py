from wtforms import form, fields, validators

from . import db
from .model import User


class LoginForm(form.Form):
    login = fields.StringField('帐号', validators=[validators.required()])
    password = fields.PasswordField('密码', validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('帐号无效')

        if not user.password == self.password.data:
            raise validators.ValidationError('密码无效')

    def get_user(self):
        return User.query.filter_by(login=self.login.data).first()
