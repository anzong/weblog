from datetime import datetime
from os import path
from random import randrange
import os

from flask import url_for, request, jsonify

from . import app


@app.route('/ckupload', methods=['POST'])
def ckupload():
    if 'upload' not in request.files:
        return upload_fail(message='不满足上传条件')

    fileobj = request.files['upload']
    extension = fileobj.filename.split('.')[1].lower()
    if extension not in ['jpg', 'gif', 'png', 'jpeg', 'svg']:
        return upload_fail(message='只能上传图片')

    fname, fext = path.splitext(fileobj.filename)

    rnd_name = '%s%s' % (gen_rnd_filename(), fext)
    filepath = path.join(
        app.static_folder, app.config['UPLOAD_PATH'], rnd_name)
    dirname = path.dirname(filepath)

    if not path.exists(dirname):
        try:
            os.makedirs(dirname)
        except:
            return upload_fail(message='创建文件目录失败')

    if not os.access(dirname, os.W_OK):
        return upload_fail(message='文件目录不可访问')

    fileobj.save(filepath)
    url = url_for('static', filename='%s/%s' % ('upload', rnd_name))

    return upload_success(url, fname)


def gen_rnd_filename():
    filename_prefix = datetime.now().strftime('%Y%m%d%H%M%S')
    return '%s%s' % (filename_prefix, str(randrange(1000, 10000)))


def upload_fail(message='上传失败'):
    return jsonify(uploaded=0, error={'message': message})


def upload_success(url, filename=''):
    return jsonify(uploaded=1, url=url, filename=filename)
