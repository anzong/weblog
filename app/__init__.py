import pytz

from flask import Flask

from flask_babelex import Babel
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# from_object() 将会寻找给定的对象(如果它是一个字符串，则会导入它)， 搜寻里面定义的全部大写的变量
app.config.from_object('config')

db = SQLAlchemy(app)
# 引入中文flask_admin
babel = Babel(app)


def format_datetime(value, fmt='F'):
    '''格式化模板日期
    '''
    tz = pytz.timezone(app.config['BABEL_DEFAULT_TIMEZONE'])
    utc = pytz.timezone('UTC')
    value = utc.localize(value, is_dst=None).astimezone(pytz.utc)
    value = value.astimezone(tz)

    if fmt == 'F':
        return value.strftime('%Y/%m/%d %H:%M:%S')
    elif fmt == 'M':
        return value.strftime('%Y/%m/%d')


app.jinja_env.filters['datetime'] = format_datetime

# 引入控制器, 管理视图
from . import modelview
from . import uploader
from . import handler
from . import reading
from . import controller
