from flask import render_template, request, send_from_directory, abort, jsonify, redirect
from sqlalchemy import func, extract
from jinja2 import TemplateNotFound

from . import app, db
from .model import Article, Topic, Sentence, Comment, ArticleStatistics

# TODO 可以启用BluePrint


@app.route('/')
def index():
    '''首页
    '''
    latests = db.session.query(Article.id, Article.createtime, Article.title).filter_by(valid=True).order_by(
        Article.createtime.desc()).limit(8).all()
    article = Article.query.get_or_404(latests[0].id)

    sentence = Sentence.query.order_by(
        Sentence.createtime.desc()).limit(1).first()

    del latests[0]
    return render_template('index.html', article=article, latests=latests, sentence=sentence)


@app.route('/robots.txt')
@app.route('/favicon.ico')
def static_from_root():
    '''根文件访问
    '''
    return send_from_directory(''.join((app.static_folder, '/root')), request.path[1:])


@app.route('/archive/')
def to_archive():
    '''归档
    '''

    articles = db.session.query(Article.id, Article.title, Article.createtime,
                                extract('year', Article.createtime).label('year')).filter_by(
        valid=True).order_by(Article.createtime.desc()).limit(20).all()

    count = Article.query.filter_by(valid=True).count()

    topics = db.session.query(Topic.id, Topic.name, func.count(Topic.id).label('count')).join(
        Article).filter_by(valid=True).group_by(Article.topic_id).all()

    return render_template('archive.html', articles=articles, topics=topics, count=count)


@app.route('/archive/<int:id>')
def to_archive_by(id):
    '''根据ID查分类
    '''

    topic = Topic.query.get_or_404(id)
    articles = db.session.query(Article.id, Article.title, Article.createtime,
                                extract('year', Article.createtime).label('year')).filter_by(
        topic=topic, valid=True).order_by(Article.createtime.desc()).all()

    count = Article.query.filter_by(topic=topic, valid=True).count()

    topics = db.session.query(Topic.id, Topic.name, func.count(Topic.id).label('count')).join(
        Article).filter_by(valid=True).group_by(Article.topic_id).all()

    return render_template('archive.html', articles=articles, topics=topics, current=topic, count=count)


@app.route('/sentence/')
def to_sentence():
    '''句子
    '''
    sentences = Sentence.query.order_by(
        Sentence.createtime.desc()).limit(20).all()
    return render_template('sentence.html', sentences=sentences)


@app.route('/article/<int:aid>')
def to_article_by(aid):
    '''文章
    '''
    article = Article.query.filter_by(id=aid, valid=True).first_or_404()
    comments = Comment.query.filter_by(
        article_id=aid).order_by(Comment.createtime).all()

    # 相关文章（随机推荐5篇同类文章）
    similers = db.session.query(Article.id, Article.title, Article.createtime).filter(
        Article.topic_id == article.topic_id, Article.id != aid, Article.valid == True).order_by(
            func.random()).limit(5).all()

    # 为确保安全，先检查对象是否存在
    if article.statistics is not None:
        article.statistics.viewcount += 1

    return render_template('article.html', title=article.title, article=article, comments=comments, similers=similers)


@app.route('/article/like')
def like_article():
    '''点赞
    '''
    aid = request.args['id']
    statistics = ArticleStatistics.query.get_or_404(aid)
    statistics.likecount += 1

    return jsonify({'count': statistics.likecount})


@app.route('/greeting/<name>')
def to_greeting(name):
    '''问候
    '''
    try:
        return render_template('greeting/%s.html' % name)
    except TemplateNotFound:
        abort(404)


@app.route('/article/comment', methods=['POST'])
def comment():
    '''提交评论
    '''
    # 判断时间戳是否存在
    if not request.form['timestamp']:
        abort(403)

    # 判断文章是否存在
    articleid = request.form['articleid']
    article = Article.query.get_or_404(articleid)

    comment = Comment()
    comment.addr = request.remote_addr
    comment.article_id = request.form['articleid']
    comment.name = request.form['name']
    comment.content = request.form['content']

    db.session.add(comment)
    db.session.commit()

    # 重定向到文章页面
    return redirect('/article/%d' % article.id)
