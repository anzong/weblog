from app import app

if __name__ == '__main__':
    app.run(host=app.config['APP_IP'], port=app.config['APP_PORT'])
