import threading
from datetime import datetime
from multiprocessing import Value

from flask import render_template, request, abort, session

from . import app

counter = Value('I', 0)
VISITOR_COUNT = 'VISITOR_COUNT'
VISITOR_TXT = 'visitor.txt'

visitors = []
lock = threading.Lock()


@app.before_first_request
def before_first_request():
    '''首次请求，加载数据
    '''
    first_load_visitor_counter()


@app.before_request
def before_request():
    if request.path.endswith('.php'):
        abort(400, 'Your malicious access has been blocked.')


@app.after_request
def after_request(resp):
    '''处理请求
    '''
    if not resp.status_code == 200:
        return resp

    if VISITOR_COUNT not in session:
        record_visitor_ip(request.remote_addr)
        increase_visitor_counter()
        save_visitor_counter()

    session[VISITOR_COUNT] = counter.value
    return resp


@app.route('/visitors', methods=['GET'])
def get_visitors():
    '''获取访问列表
    '''
    begin_index = len(visitors) - 50 if len(visitors) > 50 else 0
    visitors_slice = visitors[begin_index:]
    return render_template('visitors.html', visitors=visitors_slice[::-1])


@app.errorhandler(400)
def bad_request(error):
    return render_template('error.html', error=error), error.code


@app.errorhandler(403)
def access_forbidden(error):
    return render_template('error.html', error=error), error.code


@app.errorhandler(404)
def page_not_found(error):
    return render_template('error.html', error=error), error.code


@app.errorhandler(405)
def internal_server_error(error):
    return render_template('error.html', error=error), error.code


@app.errorhandler(500)
def method_not_allowed(error):
    return render_template('error.html', error=error), error.code


def increase_visitor_counter():
    '''增加访问计数
    '''
    with counter.get_lock():
        counter.value += 1


def save_visitor_counter():
    '''保存访问计数
    '''
    with counter.get_lock():
        with open(VISITOR_TXT, 'w') as f:
            f.seek(0)
            f.write(str(counter.value))
            f.truncate()


def first_load_visitor_counter():
    ''' 加载访问计数
    '''
    with counter.get_lock():
        with open(VISITOR_TXT, 'r') as f:
            count = f.read()
            counter.value = 0 if count == '' else int(count)


def is_include_ip(ip):
    for v in visitors:
        if v[0] == ip:
            return True
    return False


def record_visitor_ip(ip):
    '''存入IP
    '''
    if is_include_ip(ip):
        return

    try:
        lock.acquire()
        if len(visitors) >= 100:
            visitors.pop(0)
        visitors.append((ip, datetime.now()))
    finally:
        lock.release()
