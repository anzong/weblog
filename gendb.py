# 首次执行，用于生成初始数据库

from sys import argv
from time import sleep

from app import db
from app.model import User, Topic, Article, Comment, ArticleStatistics


def run_cmd(arg):
    if arg == '-create':
        db.drop_all()
        db.create_all()

    elif arg == '-init':
        init_db()

    elif arg == '-query':
        print(User.query.all())


def init_db():
    user = User()
    user.login = 'admin'
    user.name = '管理员'
    user.password = '123456'
    db.session.add(user)

    topic = Topic()
    topic.name = '默认分类'
    db.session.add(topic)

    article = Article()
    article.title = '第一篇文章'
    article.content = '这是我的第一篇文章'
    article.tag = '文章'
    article.valid = True
    article.topic = topic
    db.session.add(article)

    comment = Comment()
    comment.article_id = 1  # 第一条肯定是1
    comment.addr = '127.0.0.1'
    comment.name = '测试评论员'
    comment.content = '第一条评论，哈哈'
    db.session.add(comment)

    db.session.commit()

    statistics = ArticleStatistics()
    statistics.article_id = article.id
    db.session.add(statistics)
    db.session.commit()


if __name__ == '__main__':
    if (len(argv) == 2):
        run_cmd(argv[1].strip())
    else:
        print('-create create a database')
        print('-init   init data for the database')
        print('-query  query the init data')
