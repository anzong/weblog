DEBUG = True

SECRET_KEY = 'mynameishaha'

SQLALCHEMY_DATABASE_URI = 'sqlite:///data.sqlite'
SQLALCHEMY_COMMIT_ON_TEARDOWN = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

# flask-admin 显示语言, zh-CN en
BABEL_DEFAULT_LOCALE = 'zh_CN'
BABEL_DEFAULT_TIMEZONE = 'Asia/Shanghai'

# flask 配置 json 显示
JSON_AS_ASCII = False

# 限制上传文件为1MB
MAX_CONTENT_LENGTH = 1 * 1024 * 1024
UPLOAD_PATH = 'upload'

# 开始--自定义配置项

APP_IP = '127.0.0.1'
APP_PORT = 7456


APP_NAME = '清风徐来'
AUTHOR = '麻蛋洛夫斯基'
VERSION = '1.0.1'

CREATE_DATE = '2018/07/30'
UPDATE_DATE = '2020/08/04'

# 结束--自定义配置项
