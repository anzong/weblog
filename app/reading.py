# ========================================
# 阅读模块
# ========================================

from flask import render_template, request, abort

from re import sub, compile
from os import listdir, path
from datetime import datetime

from . import app

CHAPTER_PATTERN = compile(
    r'^(\s*第(\d{1,5}|[一二三四五六七八九十零百千万]{1,7})(卷|部))?\s*第(\d{1,5}|[一二三四五六七八九十零百千万两]{1,7})(章|节)')


def charpter_split(filepath, chapterspath, force):

    # 避免重复拆分
    if path.exists(chapterspath):
        if force:
            print('tips：章节信息已存在，即将强制执行')
        else:
            print('tips：章节信息已存在，无需处理')
            return

    starttime = datetime.now()
    filesize = path.getsize(filepath)/(1024*1024)
    print('1...开始提取章节信息（%.2f MB）... %s' % (filesize, filepath))
    chapters = extract_chapter(filepath)

    print('2...提取章节信息完成，共%d章' % len(chapters))
    print('3...开始保存章节信息')
    save_chapter(chapters, chapterspath)
    print('4...保存章节信息完成', chapterspath)

    endtime = datetime.now()
    print('5...处理完成共耗时%d秒' % (endtime - starttime).seconds)


def save_chapter(chapters, chapterspath):
    with open(chapterspath, encoding='utf-8', mode='w+') as f:
        for c in chapters:
            f.write(c['title'])
            f.write('|')
            f.write(str(c['offset']))
            f.write('|')
            f.write(str(c['size']))
            f.write('\n')
    pass


def extract_chapter(filepath):
    ''' 读取文件，提取章节基本信息
    '''

    result = []

    # 拆分章节标题及 offset
    filesize = path.getsize(filepath)
    with open(filepath, encoding='utf-8', mode='r') as f:
        size = 0

        while True:
            offset = f.tell()
            # 末尾
            if offset == filesize:
                break

            line = f.readline()
            size += len(line)

            if CHAPTER_PATTERN.search(line) is not None:
                result.append(
                    {'title': line.strip(), 'offset': offset, 'words': len(line), 'size': size})
                size = 0

    # 插入前言
    result.insert(0, {
        'title': '前言',
        'offset': 0,
        'words': 0,
        'size': result[0]['size'] - result[0]['words']
    })

    # 更新章节 size
    for i in range(len(result)):
        if i == len(result)-1:
            result[i]['size'] = result[i]['words'] + size
        else:
            result[i]['size'] = result[i]['words'] + \
                (result[i+1]['size'] - result[i+1]['words'])

    return result


@app.route('/reading/')
def to_reading():
    '''跳转到阅读主页面
    '''
    # 加载目录下所有 txt 文件的基本信息
    readingdir = app.static_folder + '/txt/'
    files = []

    for f in listdir(readingdir):
        if not f.lower().endswith('.txt'):
            continue

        filepath = readingdir + f
        size = path.getsize(filepath)
        ctime = datetime.fromtimestamp(path.getctime(filepath))

        if size < 1024:
            unit = 'B'
        elif size < 1024*1024:
            size = round(size / 1024, 1)
            unit = 'KB'
        else:
            size = round(size/(1024*1024), 1)
            unit = 'MB'

        files.append({
            'size': size,
            'unit': unit,
            'name': f[:len(f)-4],
            'ctime': ctime,
        })

    return render_template('reading/list.html', files=files)


@app.route('/reading/<name>')
def reading_content(name):
    '''读取文章
    '''

    index = request.args.get('index')
    if index is None:
        index = 0
    index = int(index) if index else 0

    chapter = _do_reading_chapter(name, index, 1)

    offset = chapter[0][0]["offset"]
    pagesize = chapter[0][0]["pagesize"]
    chaptercount = chapter[1]

    # 获取文件大小
    filepath = app.static_folder + '/txt/' + name + '.txt'
    if not path.exists(filepath):
        abort(404)

    # 读取文章
    with open(filepath, encoding='utf-8', mode='r') as f:
        if offset > 0:
            f.seek(offset)
        # 读取内容
        try:
            contentarray = f.read(pagesize)
        except UnicodeDecodeError:
            abort(500)

        offset = f.tell()

    content = ''.join(contentarray).replace('\u3000', '')
    content = sub(r'\n+', '<br>', content)
    document = {
        'name': name,
        'content': content,
        'index': index + 1,
        'pagesize': pagesize,
        'chaptercount': chaptercount,
    }
    return render_template('reading/document.html', doc=document)


@app.route('/reading/<name>/chapter')
def reading_chapter(name):
    # 取出 index
    index = request.args.get('index')
    if index is None:
        index = 0
    index = int(index) if index else 0

    pagesize = 100
    chapter = _do_reading_chapter(name, index, pagesize)

    document = {
        'name': name,
        'index': index + 1,
        'chapters': chapter[0],
        'chaptercount': chapter[1],
        'pagesize': pagesize,
    }

    return render_template('reading/document.html', doc=document)


def _do_reading_chapter(name, index, pagesize):
    '''加载章节信息
    '''

    # 获取文件大小
    chapterpath = app.static_folder + '/txt/chapter/' + name + '.txt'
    if not path.exists(chapterpath):
        # 章节信息不存在，生成章节信息
        # 懒 拆分章节会花费不少时间
        filepath = app.static_folder + '/txt/'+name+'.txt'
        charpter_split(filepath, chapterpath, False)
        # abort(405)

    # 读取章节信息
    contents = []
    with open(chapterpath, encoding='utf-8', mode='r') as f:
        lines = f.readlines()
        linecount = len(lines)
        strartindex = index * pagesize
        endindex = len(lines) if (pagesize + strartindex >
                                  len(lines)) else pagesize + strartindex

        chapterindex = strartindex
        for line in lines[strartindex:endindex]:
            array = line.split('|')
            contents.append({
                'index': chapterindex,
                'title': array[0].strip(),
                'offset': int(array[1].strip()),
                'pagesize': int(array[2].strip()),
            })
            chapterindex += 1

    return (contents, linecount)
