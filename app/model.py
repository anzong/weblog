from datetime import datetime
from bs4 import BeautifulSoup, Tag

from . import db


class TimestampMixins(object):
    '''创建时间，更新时间
    '''
    createtime = db.Column(
        db.DateTime, default=datetime.utcnow, nullable=False)
    updatetime = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class User(db.Model, TimestampMixins):
    '''用户
    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    login = db.Column(db.String(16), unique=True, nullable=False)
    password = db.Column(db.String(32))

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __unicode__(self):
        return self.name


class Topic(db.Model, TimestampMixins):
    '''分类
    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    createtime = db.Column(
        db.DateTime, default=datetime.utcnow, nullable=False)
    updatetime = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return self.name


class Article(db.Model, TimestampMixins):
    '''文章
    '''
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), unique=True, nullable=False)
    content = db.Column(db.Text, nullable=True)
    tag = db.Column(db.String(100), nullable=True)
    valid = db.Column(db.Boolean, default=False, nullable=False)

    topic_id = db.Column(db.Integer, db.ForeignKey('topic.id'), nullable=False)
    topic = db.relationship('Topic')

    # 一对一映射
    statistics = db.relationship(
        'ArticleStatistics', uselist=False, backref=db.backref('article', lazy='joined'))

    def __repr__(self):
        return self.title

    def html(self, snapshot=False):
        '''获取网页内容，并自动追加“目录”
        '''
        if snapshot and len(self.content) > 50:
            return '%s......' % self.content[:50]

        h = ('h1', 'h2', 'h3', 'h4', 'h5', 'h6')

        bs = BeautifulSoup(self.content, 'html.parser')
        ul_format = '<ul id="article-menu">%s</ul>\n'
        li_format = '<li class="item%d"><a href="#%s">%s</a></li>\n'
        table = []
        idx = 0

        for child in bs.children:
            if not type(child) == Tag:  # 只获取Tag
                continue

            if child.name in h:
                idx += 1
                child['id'] = 'tag%d' % idx
                table.append(child)

        h_size = []
        for t in table:
            h_size.append(int(t.name[1:]))

        min_h_size = min(h_size) if len(h_size) else 0
        li_list = []

        for t in table:
            offset = int(t.name[1:]) - min_h_size + 1
            li_list.append(li_format % (offset, t['id'], t.get_text()))

        table_of_contents = ul_format % ''.join(
            li_list) if len(li_list) else ''
        return (table_of_contents, str(bs))

    def summary(self, length=50):
        '''获取摘要
        '''
        summary_text = ''
        summary_html = ''
        bs = BeautifulSoup(self.content, 'html.parser')

        for child in bs.children:
            if len(summary_text) > length:
                break
            if not type(child) == Tag:  # 只获取Tag
                continue

            summary_html = ''.join((summary_html, str(child)))
            summary_text = ''.join((summary_text, child.get_text() or ''))

        return summary_html


class ArticleStatistics(db.Model, TimestampMixins):
    '''文章统计
    '''
    article_id = db.Column(db.Integer, db.ForeignKey(
        'article.id'), primary_key=True, autoincrement=False)

    viewcount = db.Column(db.Integer, default=0, nullable=False)
    likecount = db.Column(db.Integer, default=0, nullable=False)


class Sentence(db.Model, TimestampMixins):
    '''句子
    '''
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(255), nullable=False)
    author = db.Column(db.String(64), nullable=True)
    reference = db.Column(db.String(255), nullable=True)  # 出处

    def html(self, snapshot=False):
        if snapshot and len(self.content) > 50:
            return '%s......' % self.content[:50]
        else:
            return self.content


class Comment(db.Model, TimestampMixins):
    '''留言、评论
    '''
    id = db.Column(db.Integer, primary_key=True)
    article_id = db.Column(db.Integer, nullable=False)  # 不建外键
    name = db.Column(db.String(32), nullable=False)
    addr = db.Column(db.String(10), nullable=True)
    content = db.Column(db.String(1000), nullable=False)

    def article(self):
        if self.article_id is not None:
            return Article.query.get(self.article_id).title
