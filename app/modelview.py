from datetime import datetime

from flask import url_for, redirect, request
from flask_admin import Admin, AdminIndexView, expose, helpers
from flask_admin.contrib.sqla import ModelView
from flask_admin.model import typefmt
from flask_admin.model.template import macro, LinkRowAction
from wtforms import TextAreaField

import flask_login as login

from . import app, db, format_datetime
from .model import User, Topic, Article, Sentence, Comment, ArticleStatistics
from .modelform import LoginForm

MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    datetime: lambda view, value: format_datetime(value)
})


class MyAdminIndexView(AdminIndexView):

    @expose('/')
    def index(self):
        self._template_args['form'] = LoginForm()
        return super(MyAdminIndexView, self).index()

    @expose('/login', methods=('GET', 'POST'))
    def login(self):
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        self._template_args['form'] = form
        return super(MyAdminIndexView, self).index()

    @expose('/logout')
    def logout(self):
        login.logout_user()
        return redirect(url_for('admin.index'))


class MyModelView(ModelView):
    # 创建、编辑时忽略列
    form_excluded_columns = ('createtime', 'updatetime')
    column_type_formatters = MY_DEFAULT_FORMATTERS
    column_default_sort = ('createtime', True)  # True为倒序排序

    def is_accessible(self):
        return login.current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('admin.index'))


class TopicModelView(MyModelView):
    can_delete = False
    column_labels = dict(name='名称', createtime='创建时间', updatetime='更新时间')
    column_editable_list = ['name']


class ArticleModelView(MyModelView):
    can_view_details = True

    create_template = 'admin/article_create.html'
    edit_template = 'admin/article_edit.html'
    details_template = 'admin/article_details.html'

    column_formatters = dict(content=macro('render_content'))
    column_editable_list = ('topic', 'valid')
    column_list = ('topic', 'title',
                   'tag', 'createtime', 'updatetime', 'valid')

    column_labels = dict(title='标题', content='内容',
                         topic='分类', tag='标签', valid='发布', createtime='创建时间', updatetime='更新时间')

    form_excluded_columns = ('statistics', 'createtime', 'updatetime')

    def after_model_change(self, form, model, is_created):
        '''创建文章时，同时创建统计对象
        '''
        if is_created:
            model.statistics = ArticleStatistics()
            model.statistics.article_id = model.id


class SentenceModelView(MyModelView):
    can_view_details = True
    column_labels = dict(content='内容', author='作者',
                         reference='出处', createtime='创建时间', updatetime='更新时间')
    form_overrides = {
        'content': TextAreaField
    }

    form_widget_args = {
        'content': {'rows': 6}
    }


class CommentModelView(MyModelView):
    can_view_details = True
    can_create = False
    can_edit = False

    details_template = 'admin/comment_details.html'

    column_labels = dict(content='内容', name='昵称', article_id='文章ID',
                         addr='地址', createtime='创建时间', updatetime='更新时间')

    column_list = ['name', 'content', 'article_id', 'addr', 'createtime']

    column_extra_row_actions = [
        LinkRowAction('glyphicon glyphicon-info-sign',
                      '{row_id}', '查看文章')
    ]

    @expose("/<int:id>")
    def to_article(self, id):
        comment = Comment.query.get_or_404(id)
        return redirect('%s%s%s' % (url_for('article.details_view'), '?id=', comment.article_id))


def init_login():
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(user_id)


# 初始化Flask-Login
init_login()

admin = Admin(app, name=app.config['APP_NAME'],
              template_mode='bootstrap3', base_template='admin/mybase.html', index_view=MyAdminIndexView())
admin.add_view(TopicModelView(Topic, db.session, name='分类'))
admin.add_view(SentenceModelView(Sentence, db.session, name='句子'))
admin.add_view(ArticleModelView(Article, db.session, name='文章'))
admin.add_view(CommentModelView(Comment, db.session, name='评论'))
